# CharaList
Chara is the to be backend for charalist aka the character creator directory, a yet to go live site, currently in _very_ early alpha with most features missing or dysfunctional, and is likely to undergo large amounts of code churn.

## Hosting
As it is in its current state its not really intended for selfhosting, but if you'd like to host it you'll need go1.18 and postgresql, and you'll need to manually run the sql in database/schema/ to configure the database, at which point you can run it via `go run ./cmd/server <psql connection string>`

## License
This software, including all source files in this repository unless explicitly stated otherwise, is licensed under the AGPL-3.0 as can be found in `LICENSE.md`. 