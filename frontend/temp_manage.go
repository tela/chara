package frontend

/*
import (
	"fmt"
	"html/template"
	"math"
	"net/http"
	"net/url"
	"strconv"
	"strings"

	"codeberg.org/tela/chara"
	"codeberg.org/tela/chara/database/models"
)

func (f *Frontend) epRoot(w http.ResponseWriter, r *http.Request) (template.HTML, error) {
	w.Header().Set("content-type", "text/html")
	return `<form action="/search?"><input type="search" id="search" name="search"></form>`, nil
}

func (f *Frontend) epSearch(w http.ResponseWriter, r *http.Request) (template.HTML, error) {
	vs := strings.Fields(r.URL.Query().Get("search"))
	nid := ""
	for _, x := range vs {
		tstr, err := url.QueryUnescape(x)
		if err != nil {
			return "", err
		}
		if strings.HasPrefix(tstr, "#") {
			tag, err := chara.ParseTagID(tstr[1:])
			if err != nil {
				return "", err
			}

			nid += " + " + strconv.Itoa(int(tag))
		} else {
			fmt.Println(tstr)
			tag, err := f.b.Database.GetTagByName(r.Context(), tstr)
			if err != nil {
				return "", fmt.Errorf("gtbn %w", err)
			}
			nid += " + " + strconv.FormatInt(tag.ID, 10)
		}
	}
	nid = nid[3:]
	fmt.Println(nid)

	results, err := f.b.Database.SearchTags(r.Context(), models.SearchTagsParams{
		Query: nid,
		Limit: 100,
	})
	if err != nil {
		return "", err
	}
	w.Header().Set("content-type", "text/html")
	for _, x := range results {
		fmt.Fprintf(w, "<p>ID: %v Type: %v Remote: %v</p>", chara.MakerID(x.ID), x.Type, x.RemoteID)
	}
	return "", nil
}

func (f *Frontend) listTags(w http.ResponseWriter, r *http.Request) (template.HTML, error) {
	t, err := f.b.Database.ListTagsByID(r.Context(), models.ListTagsByIDParams{
		ID:    math.MaxInt64,
		Limit: 9999,
	})
	if err != nil {
		return "", err
	}

	w.Header().Set("content-type", "text/html")
	for _, x := range t {
		fmt.Fprintf(w, "<li>ID: %v NAME: %v %v</li>", chara.TagID(x.ID), x.Name, x.Aliases)
	}
	return "", nil
}

func (f *Frontend) newTag(w http.ResponseWriter, r *http.Request) (template.HTML, error) {
	err := f.b.Database.NewTag(r.Context(), models.NewTagParams{
		ID:      int64(chara.NewTagID()),
		Name:    r.URL.Query().Get("name"),
		Aliases: []string{},
	})
	if err != nil {
		return "", err
	}
	w.Header().Set("content-type", "text/html")
	return "<p>:)</p>", nil
}

func (f *Frontend) listMakers(w http.ResponseWriter, r *http.Request) (template.HTML, error) {
	t, err := f.b.Database.ListMakersByID(r.Context(), models.ListMakersByIDParams{
		ID:    math.MaxInt64,
		Limit: 9999,
	})
	if err != nil {
		return "", err
	}

	w.Header().Set("content-type", "text/html")
	for _, x := range t {
		fmt.Fprintf(w, "<p>ID: %v Type: %v Remote: %v</p>", chara.MakerID(x.ID), x.Type, x.RemoteID)
	}
	return "", nil
}

func (f *Frontend) newMaker(w http.ResponseWriter, r *http.Request) (template.HTML, error) {
	err := f.b.Database.NewMaker(r.Context(), models.NewMakerParams{
		ID:       int64(chara.NewMakerID()),
		Type:     int16(chara.MakerTypeOther),
		Remoteid: r.URL.Query().Get("remote"),
	})
	if err != nil {
		return "", err
	}
	w.Header().Set("content-type", "text/html")
	return "<p>:)</p>", nil
}

func (f *Frontend) assignMakerTag(w http.ResponseWriter, r *http.Request) (template.HTML, error) {
	mid, err := chara.ParseMakerID(r.URL.Query().Get("maker"))
	if err != nil {
		return "", err
	}
	tid, err := chara.ParseTagID(r.URL.Query().Get("tag"))
	if err != nil {
		return "", err
	}
	positive := r.URL.Query().Get("positive") == "true"
	err = f.b.Database.AssignTag(r.Context(), models.AssignTagParams{
		MakerID:  int64(mid),
		TagID:    int64(tid),
		Positive: positive,
	})
	if err != nil {
		return "", err
	}
	w.Header().Set("content-type", "text/html")
	return ":3", nil
}
*/
