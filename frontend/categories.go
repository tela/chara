package frontend

import (
	"html/template"
	"strings"

	"codeberg.org/tela/chara"
	"github.com/go-chi/chi/v5"
)

func (f *Frontend) categoryRoutes(r chi.Router) {
	r.Get("/category", f.middleware(f.getCategory))
	r.Get("/category/{id}", f.middleware(f.getCategoryID))
	r.Get("/category/all", f.middleware(f.getCategoryAll))
	r.Get("/category/create", f.middleware(f.getCategoryCreate))
	r.Post("/category/create", f.middleware(f.postCategoryCreate))
}

// GET /category
func (f *Frontend) getCategory(ctx *mwContext) (template.HTML, error) {

	type data struct {
	}

	return callTempl(ctx.templCtx, "get /category", templData[data]{
		Title: "category",
		Data:  data{},
	})
}

// GET /category/ID
func (f *Frontend) getCategoryID(ctx *mwContext) (template.HTML, error) {
	params := struct {
		ID chara.CategoryID `form:"id"`
	}{}
	if ok := ctx.parseParams(&params); !ok {
		return "", nil
	}

	cat, err := f.b.CategoryGet(ctx.Context, params.ID)
	if err != nil {
		return "", err
	}

	type data struct {
		Category *chara.Category
	}

	return callTempl(ctx.templCtx, "get /category/id", templData[data]{
		Title: "categoryID",
		Data: data{
			Category: cat,
		},
	})
}

// GET /category/all
func (f *Frontend) getCategoryAll(ctx *mwContext) (template.HTML, error) {

	type data struct {
	}

	return callTempl(ctx.templCtx, "get /category/all", templData[data]{
		Title: "categoryAll",
		Data:  data{},
	})
}

// GET /category/Create
func (f *Frontend) getCategoryCreate(ctx *mwContext) (template.HTML, error) {

	type data struct {
	}

	return callTempl(ctx.templCtx, "get /category/create", templData[data]{
		Title: "categoryCreate",
		Data:  data{},
	})
}

// POST /category/Create
func (f *Frontend) postCategoryCreate(ctx *mwContext) (template.HTML, error) {
	post := struct {
		Name    string           `form:"name"`
		Aliases string           `form:"aliases"`
		Parent  chara.CategoryID `form:"parent"`
	}{}
	if ok := ctx.parsePost(&post); !ok {
		return "", nil
	}

	cat, err := f.b.CategoryCreate(ctx.Context, chara.Category{
		Name:    post.Name,
		Aliases: strings.Split(post.Aliases, ","),
	})
	if err != nil {
		return "", err
	}

	type data struct {
		Category *chara.Category
	}

	return callTempl(ctx.templCtx, "post /category/create", templData[data]{
		Title: "categoryCreate",
		Data: data{
			Category: cat,
		},
	})
}
