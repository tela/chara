package frontend

import (
	"fmt"
	"html/template"
	"log"
	"math/rand"
	"net/http"
	"runtime/debug"
	"strconv"

	"codeberg.org/tela/chara/backend"
	"codeberg.org/tela/chara/internal/tracer"
	"github.com/go-chi/chi/v5"
)

type Config struct {
}

type Frontend struct {
	R chi.Router
	b *backend.Backend
}

func (f *Frontend) Init(b *backend.Backend, c Config) error {
	f.b = b
	r := chi.NewRouter()
	f.R = r

	return f.rootRoutes(r)
}

func (f *Frontend) rootRoutes(r *chi.Mux) error {
	r.Get("/", f.middleware(f.rIndex))
	r.Get("/debug/dblog/{id}", f.middleware(f.getDebugDBLogSession))
	r.Handle("/static/*",
		http.StripPrefix("/static/", http.FileServer(http.FS(staticFS))),
	)
	r.Group(f.tagsRoutes)
	r.Group(f.categoryRoutes)

	// r.Get("/", middleware(f.epRoot))
	// r.Get("/tag", middleware(f.listTags))
	// r.Get("/newtag", middleware(f.newTag))
	// r.Get("/makers", middleware(f.listMakers))
	// r.Get("/newmaker", middleware(f.newMaker))
	// r.Get("/assign", middleware(f.assignMakerTag))
	// r.Get("/search", middleware(f.epSearch))

	return nil
}

type mwContext struct {
	*backend.Context
	// will contain stuff like auth shenanigans in the future

	w http.ResponseWriter
	r *http.Request

	templCtx *templContext

	pageSession uint64

	htmx Htmx
}

type Htmx struct {
	Request        bool
	HistoryRequest bool
}

func getSession(r *http.Request) (dbgSession uint64) {
	if r.Header.Get("page-session") != "" {
		ps, err := strconv.ParseUint(r.Header.Get("page-session"), 0, 0)
		if err == nil {
			dbgSession = ps
		} else {
			log.Println(err)
		}
	} else {
		dbgSession = rand.Uint64()
	}

	return
}

func isHtmx(r *http.Request) Htmx {
	return Htmx{
		Request:        r.Header.Get("HX-Request") == "true",
		HistoryRequest: r.Header.Get("HX-History-Restore-Request") == "true",
	}
}

func (f *Frontend) middleware(fn func(ctx *mwContext) (template.HTML, error)) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		ctx := &mwContext{
			w: w, r: r,
			Context: &backend.Context{
				Context: tracer.With(r.Context()),
			},

			pageSession: getSession(r),
			htmx:        isHtmx(r),
		}

		templCtx, err := f.templContext(ctx)
		if err != nil {
			log.Println(err)
			fmt.Fprintf(w, "<p>%v</p>", err.Error())
			return
		}
		ctx.templCtx = templCtx

		dat, err := fn(ctx)
		if err != nil {
			log.Println(err)
			fmt.Fprintf(w, "<p>%v</p>", err.Error())
			return
		}

		appendDBLog(ctx.pageSession, tracer.Get(ctx.Context.Context).String())

		if dat == "" {
			return
		}
		if ctx.htmx.Request {
			w.Write([]byte(dat))
			return
		}

		type dbg struct {
			DB string

			BuildInfo string
		}

		type data struct {
			Body    template.HTML
			Debug   dbg
			Session uint64
		}

		binfo, ok := debug.ReadBuildInfo()
		binfos := ""
		if ok {
			if binfo.Main.Sum == "" {
				binfos = fmt.Sprintf("Chara %v %v", binfo.Main.Version, binfo.GoVersion)
			} else {
				binfos = fmt.Sprintf("Chara %v (%v) %v", binfo.Main.Version, binfo.Main.Sum, binfo.GoVersion)
			}
		}

		dat, err = callTempl(templCtx, "meta wrapper", templData[data]{
			Data: data{
				Body: dat,
				Debug: dbg{
					DB:        tracer.Get(ctx.Context.Context).String(),
					BuildInfo: binfos,
				},
				Session: ctx.pageSession,
			},
		})
		if err != nil {
			log.Println(err)
			return
		}

		w.Write([]byte(dat))
	}
}
