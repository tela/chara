package frontend

import (
	"html/template"
	"log"
	"net/http"
	"strings"
	"sync"
	"time"

	"github.com/AspieSoft/go-ttlcache"
)

var dblogMu sync.RWMutex
var dbLog = ttlcache.New[uint64, string](time.Minute*5, time.Minute)

func appendDBLog(session uint64, add string) {
	dblogMu.Lock()
	defer dblogMu.Unlock()

	str, ok := dbLog.Get(session)
	if ok {
		str += "\n\n"
	}
	if add != "" {
		str += "- " + time.Now().Format(time.StampMicro) + "\n"
	}
	str += add
	dbLog.Set(session, str)
}

// GET /Debug/DBLog/Session
func (f *Frontend) getDebugDBLogSession(ctx *mwContext) (template.HTML, error) {
	params := struct {
		ID uint64 `form:"id"`
	}{}
	if ok := ctx.parseParams(&params); !ok {
		return "", nil
	}

	// log.Println(params.ID)
	ctx.w.Header().Set("content-type", "text/event-stream")
	ctx.w.Write([]byte("data: event init\n\n"))

	var prev string

	toilet, flushable := ctx.w.(http.Flusher)

	ticker := time.NewTicker(time.Second)
loop:
	for {
		select {
		case <-ticker.C:
		case <-ctx.r.Context().Done():
			log.Println(ctx.r.Context().Err())
			ticker.Stop()
			break loop
		}
		ctx.w.Write([]byte{':', ' ', '\n'})

		dbLog.Touch(params.ID)

		str, ok := dbLog.Get(params.ID)
		if !ok || str == prev {
			continue
		}
		prev = str

		pass := "event: dbquery\n" + "data: " + strings.ReplaceAll(str, "\n", "\ndata: ") + "\n\n"
		// fmt.Println(pass)

		ctx.w.Write([]byte(pass))

		if flushable {
			toilet.Flush()
		}
	}
	return "", nil
}
