package frontend

import (
	"bytes"
	"embed"
	"fmt"
	"hash/fnv"
	"html/template"
	"io"
	"io/fs"
	"log"
	"os"
	"path/filepath"
	"runtime"
	"strings"
	"time"

	"codeberg.org/tela/chara"
	"github.com/Masterminds/sprig/v3"
)

type templAuth struct {
	UserID      chara.UserID
	Permissions chara.Permissions
}

type templContext struct {
	Auth templAuth

	Htmx bool
}

type templData[T any] struct {
	*templContext

	Title string
	Data  T
}

func callTempl[T any](ctx *templContext, name string, meta templData[T]) (template.HTML, error) {
	buf := new(bytes.Buffer)

	meta.templContext = ctx

	err := templ.ExecuteTemplate(buf, name, meta)
	return template.HTML(buf.String()), err
}

var templ = template.Must(templParse(embedFS))

func templParse(fs fs.FS) (*template.Template, error) {
	return template.New("").
		Funcs(sprig.HtmlFuncMap()).
		Funcs(templFuncs).
		ParseFS(fs, "templ/*/*.html")
}

//go:embed templ/*/*.html static/*
var embedFS embed.FS

var dataFS = fs.FS(embedFS)
var staticFS = must(fs.Sub(embedFS, "static"))

func must[T any](t T, err error) T {
	if err != nil {
		panic(err)
	}
	return t
}

var notfirstupdate bool

// devtest spaghetti
func init() {
	// TODO: for now, change this later
	if false && os.Getenv("DEV") == "" {
		return
	}

	_, file, _, ok := runtime.Caller(0)
	if !ok {
		return
	}
	dir := strings.TrimSuffix(file, "templates.go")
	fmt.Println(dir)

	dataFS = os.DirFS(dir)
	staticFS = os.DirFS(filepath.Join(dir, "static"))

	hashes := make(map[string][]byte)

	hash := func() bool {
		dif := false

		err := fs.WalkDir(dataFS, "templ", func(path string, d fs.DirEntry, err error) error {
			if err != nil {
				return err
			}
			if d.IsDir() {
				return nil
			}
			if ok, _ := filepath.Match("templ/*/*.html", path); !ok {
				return nil
			}

			dat, err := os.Open(dir + "/" + path)
			if err != nil {
				log.Println(err, path)
				return err
			}

			h := fnv.New128()
			io.Copy(h, dat)
			dat.Close()

			var b []byte
			b = h.Sum(b)

			if !bytes.Equal(hashes[path], b) {
				if notfirstupdate {
					log.Println(filepath.Base(filepath.Dir(path))+"/"+d.Name(), "changed, reloading templates")
				}
				dif = true
			}
			hashes[path] = b

			return nil
		})
		if err != nil {
			log.Println(err)
		}

		return dif
	}

	hash()

	notfirstupdate = true

	go func() {
		for {
			time.Sleep(time.Second)

			ok := hash()
			if !ok {
				continue
			}

			temp, err := templParse(dataFS)
			if err != nil {
				log.Println("failed to reload templates:", err)
				continue
			}
			templ = temp

			log.Println("reloaded templates")
		}
	}()
}
