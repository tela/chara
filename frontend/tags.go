package frontend

import (
	"fmt"
	"html/template"
	"strings"

	"codeberg.org/tela/chara"
	"github.com/go-chi/chi/v5"
)

func (f *Frontend) tagsRoutes(r chi.Router) {
	r.Get("/tag", f.middleware(f.getTag))
	r.Get("/tag/search", f.middleware(f.getTagSearch))
	r.Get("/tag/{id}", f.middleware(f.getTagId))
	r.Get("/tag/all", f.middleware(f.getTagAll))
	r.Get("/tag/create", f.middleware(f.getTagCreate))
	r.Post("/tag/create", f.middleware(f.postTagCreate))
}

// GET /tag
func (f *Frontend) getTag(ctx *mwContext) (template.HTML, error) {

	type data struct {
	}

	return callTempl(ctx.templCtx, "get /tag", templData[data]{
		Title: "Tags",
		Data:  data{},
	})
}

// GET TagsSearch
func (f *Frontend) getTagSearch(ctx *mwContext) (template.HTML, error) {
	fmt.Println(ctx.r.URL)
	ctx.r.ParseForm()
	fmt.Println(ctx.r.PostForm)

	get := struct {
		Query  string `form:"q"`
		Limit  int    `form:"max"`
		Prefix bool   `form:"prefix"`
	}{}
	if ok := ctx.parseGet(&get); !ok {
		return "", nil
	}

	if get.Limit == 0 {
		get.Limit = 10
	}

	get.Query += "%"
	if !get.Prefix {
		get.Query = "%" + get.Query
	}
	get.Query = strings.ReplaceAll(get.Query, "_", "\\_")

	fmt.Println(get.Query, get.Limit, get.Prefix)

	tags, err := f.b.TagSearch(ctx.Context, get.Query, get.Limit)
	if err != nil {
		return "", err
	}
	fmt.Println(tags)

	type data struct {
		Tags []*chara.Tag
	}

	return callTempl(ctx.templCtx, "get /tag/search", templData[data]{
		Title: "TagsSearch",
		Data: data{
			Tags: tags,
		},
	})
}

// GET /tag/id
func (f *Frontend) getTagId(ctx *mwContext) (template.HTML, error) {
	params := struct {
		ID chara.TagID `form:"id"`
	}{}
	if ok := ctx.parseParams(&params); !ok {
		return "", nil
	}

	tag, err := f.b.TagGet(ctx.Context, params.ID)
	if err != nil {
		return "", err
	}

	type data struct {
		Tag *chara.Tag
	}

	return callTempl(ctx.templCtx, "get /tag/id", templData[data]{
		Title: "TagsId",
		Data: data{
			Tag: tag,
		},
	})
}

// GET /tag/create
func (f *Frontend) getTagCreate(ctx *mwContext) (template.HTML, error) {

	type data struct {
	}

	return callTempl(ctx.templCtx, "get /tag/create", templData[data]{
		Title: "TagsCreate",
		Data:  data{},
	})
}

// POST /tag/create
func (f *Frontend) postTagCreate(ctx *mwContext) (template.HTML, error) {
	post := struct {
		Name     string           `form:"name"`
		Aliases  string           `form:"aliases"`
		Category chara.CategoryID `form:"category"`
	}{}
	if ok := ctx.parsePost(&post); !ok {
		return "", nil
	}

	tag, err := f.b.TagCreate(ctx.Context, post.Name, strings.Split(post.Aliases, " "), post.Category)
	if err != nil {
		return "", err
	}

	type data struct {
		Tag *chara.Tag
	}

	return callTempl(ctx.templCtx, "post /tag/create", templData[data]{
		Title: "TagsCreate",
		Data:  data{Tag: tag},
	})
}

func (f *Frontend) getTagAll(ctx *mwContext) (template.HTML, error) {
	var after chara.TagID
	if q := ctx.r.URL.Query().Get("after"); q != "" {
		at, err := chara.ParseTagID(q)
		if err != nil {
			return "", chara.NewError(chara.ErrorInputFailsValidation,
				"`"+q+"` is not a valid ID: "+err.Error(),
			)
		}
		after = at
	}

	tags, err := f.b.TagList(ctx.Context, after)
	if err != nil {
		return "", err
	}

	fmt.Println(tags)

	type data struct {
		Tags   []*chara.Tag
		LastID chara.TagID
	}

	var last chara.TagID
	if len(tags) != 0 {
		last = tags[len(tags)-1].ID
	}

	return callTempl(ctx.templCtx, "get /tag/all", templData[data]{
		Title: "all tags",
		Data: data{
			Tags:   tags,
			LastID: last,
		},
	})
}
