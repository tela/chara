package frontend

import (
	"html/template"

	"codeberg.org/tela/chara"
)

func (f *Frontend) templContext(ctx *mwContext) (*templContext, error) {
	perm, err := f.b.Auth.GetPermissions(ctx.Context.Context, ctx.UserID)
	if err != nil {
		return nil, err
	}

	tctx := &templContext{
		Auth: templAuth{
			UserID:      ctx.UserID,
			Permissions: perm,
		},

		Htmx: ctx.htmx.Request,
	}

	return tctx, nil
}

var templFuncs = template.FuncMap{
	"permissions": func() map[string]chara.Permissions {
		return chara.PermissionMap
	},
}
