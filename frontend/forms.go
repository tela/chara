package frontend

import (
	"net/http"
	"net/url"

	"github.com/go-chi/chi/v5"
	"github.com/monoculum/formam/v3"
)

func (ctx *mwContext) parseGet(get any) bool {
	err := formParser.Decode(ctx.r.URL.Query(), get)
	if err != nil {
		http.Error(ctx.w, err.Error(), http.StatusBadRequest)
		return false
	}
	return true
}

func (ctx *mwContext) parsePost(post any) bool {
	err := ctx.r.ParseForm()
	if err != nil {
		http.Error(ctx.w, err.Error(), http.StatusBadRequest)
		return false
	}

	err = formParser.Decode(ctx.r.PostForm, post)
	if err != nil {
		http.Error(ctx.w, err.Error(), http.StatusBadRequest)
		return false
	}
	return true
}

func (ctx *mwContext) parseParams(params any) bool {
	chitx := chi.RouteContext(ctx.r.Context())
	urlparams := make(url.Values)
	for i := range chitx.URLParams.Keys {
		k, v := chitx.URLParams.Keys[i], chitx.URLParams.Values[i]
		if urlparams[k] == nil {
			urlparams[k] = []string{v}
		} else {
			urlparams[k] = append(urlparams[k], v)
		}
	}

	err := formParser.Decode(urlparams, params)
	if err != nil {
		http.Error(ctx.w, err.Error(), http.StatusBadRequest)
		return false
	}
	return true
}

var formParser = formam.NewDecoder(&formam.DecoderOptions{
	TagName: "form",
})
