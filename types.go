package chara

//go:generate go run codeberg.org/jande/csid/gen -genfuncs -timefuncs -out ./ids.go CategoryID TagID MakerID UserID AuditEntryID

type Category struct {
	ID      CategoryID `json:"id"`
	Name    string     `json:"name"`
	Aliases []string   `json:"aliases"`
}

type Tag struct {
	ID         TagID      `json:"id"`
	CategoryID CategoryID `json:"category_id"`
	Name       string     `json:"name"`
	Aliases    []string   `json:"aliases"`
}

//go:generate stringer -type=MakerType
type MakerType uint16

const (
	MakerTypeOther  MakerType = iota // Other
	MakerTypePicrew                  // Picrew
)

type Maker struct {
	ID       MakerID   `json:"id"`
	Type     MakerType `json:"type"`
	RemoteID string    `json:"remote_id"`

	PositiveTags []TagID `json:"positive_tags"`
	NegativeTags []TagID `json:"negative_tags"`
}

type User struct {
	ID          UserID      `json:"id"`
	Name        string      `json:"name"`
	Permissions Permissions `json:"permissions"`
}
