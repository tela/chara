package backend

import (
	"context"

	"codeberg.org/tela/chara/backend/auth"
	"codeberg.org/tela/chara/database/models"
	"github.com/jackc/pgx/v4/pgxpool"
)

type Config struct {
	Database struct {
		ConnString string

		// Host string
		// Port uint
		// User string
		// Pass string
	}
}

type Backend struct {
	Config Config

	Auth auth.Auth

	db     *models.Queries
	pgpool *pgxpool.Pool
}

func (b *Backend) Init(ctx context.Context, c Config) error {
	b.Config = c

	pgc, err := pgxpool.ParseConfig(c.Database.ConnString)
	if err != nil {
		return err
	}
	pgpool, err := pgxpool.ConnectConfig(ctx, pgc)
	if err != nil {
		return err
	}
	err = pgpool.Ping(ctx)
	if err != nil {
		return err
	}
	b.pgpool = pgpool
	b.db = models.New(pgpool)

	return nil
}

func (b *Backend) Close(ctx context.Context) error {
	b.pgpool.Close()
	return nil
}
