package auth

import (
	"context"

	"codeberg.org/tela/chara"
)

type Auth struct {
}

func (a *Auth) UserCanDo(ctx context.Context, uid chara.UserID, requested chara.Permissions) error {
	perms, err := a.GetPermissions(ctx, uid)
	if err != nil {
		return err
	}
	if perms.Has(requested) {
		return nil
	}

	return chara.NewError(chara.ErrorMissingPermissions, perms.Missing(requested).String())
}

func (a *Auth) GetPermissions(ctx context.Context, uid chara.UserID) (has chara.Permissions, err error) {
	// TODO, this would be a terrible idea to not do
	return chara.PermissionAll, nil
}
