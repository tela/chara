package backend

import (
	"context"
	"sync"

	"codeberg.org/tela/chara"
	"codeberg.org/tela/chara/database/models"
	"codeberg.org/tela/chara/internal/tracer"
)

type Context struct {
	context.Context
	UserID chara.UserID

	query *models.Queries
}

func (c *Context) GetTx(b *Backend, transaction bool) (*models.Queries, func(bool) error, error) {
	if c.query != nil {
		return c.query, func(bool) error { return nil }, nil
	}

	if !transaction {
		c.query = b.db
		return c.query, func(bool) error { return nil }, nil
	}

	tx, err := b.pgpool.Begin(c)
	if err != nil {
		return nil, nil, err
	}
	c.query = b.db.WithTx(tx)

	on := sync.Once{}
	return c.query, func(commit bool) error {
		qtr := tracer.Get(c)
		var err error
		on.Do(func() {
			if commit {
				done := qtr.Query("getTx / commit", false)
				err = tx.Commit(c)
				done()
			} else {
				done := qtr.Query("getTx / rollback", false)
				err = tx.Rollback(c)
				done()
			}
		})
		return err
	}, nil
}

func (c *Context) getQ(b *Backend) *models.Queries {
	if c.query != nil {
		return c.query
	}
	c.query = b.db
	return c.query
}
