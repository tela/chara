package backend

import (
	"errors"
	"fmt"
	"strconv"
	"strings"

	"codeberg.org/tela/chara"
	"codeberg.org/tela/chara/database/models"
	"codeberg.org/tela/chara/internal/tracer"
	"github.com/jackc/pgx/v4"
	"golang.org/x/text/unicode/norm"
)

const (
	LimitTagNameLength = 50
	LimitTagAliasCount = 10
)

func NormaliseName(name string) string {
	name = strings.TrimSpace(name)
	name = strings.ReplaceAll(name, " ", "_")
	name = norm.NFKD.String(name)

	return name
}

func NormaliseNames(names []string) []string {
	for i := range names {
		names[i] = NormaliseName(names[i])
	}
	return names
}

func (b *Backend) TagValidateNames(name string, aliases []string) error {
	name = norm.NFKC.String(name)

	if err := ValidateNameLength(name); err != nil {
		return chara.ErrorAddCrumb(err, "name")
	}

	if err := ValidateAliasCount(aliases); err != nil {
		return chara.ErrorAddCrumb(err, "aliases")
	}

	if err := ValidateAliasLength(aliases); err != nil {
		return chara.ErrorAddCrumb(err, "aliases")
	}

	return nil
}

func ValidateNameLength(name string) error {
	if len(name) <= LimitTagNameLength {
		return nil
	}

	return chara.NewError(chara.ErrorInputFailsValidation,
		fmt.Sprintf(
			"Name (len: %v) exceeds max Length: %v",
			len(name), LimitTagNameLength,
		),
	)
}

func ValidateAliasCount(aliases []string) error {
	if len(aliases) <= LimitTagAliasCount {
		return nil
	}

	return chara.NewError(
		chara.ErrorInputFailsValidation,
		fmt.Sprintf(
			"Alias Count (len: %v) exceeds max Count: %v",
			len(aliases), LimitTagAliasCount,
		),
	)
}

func ValidateAliasLength(aliases []string) error {
	for i, alias := range aliases {
		err := ValidateNameLength(alias)
		if err == nil {
			continue
		}

		return chara.ErrorAddCrumb(err, strconv.Itoa(i))
	}

	return nil
}

func (b *Backend) CategoryExists(ctx *Context, category chara.CategoryID) error {
	qtr := tracer.Get(ctx.Context)

	done := qtr.Query("get category", false)
	_, err := b.db.GetCategory(ctx.Context, int64(category))
	done()
	// check if the category exists, give a special error if the error reports it doesn't exist
	if errors.Is(err, pgx.ErrNoRows) {
		return chara.NewError(
			chara.ErrorNotFound,
			"Invalid Category #"+category.String(),
		)
	}
	if err != nil { // an actual error
		return err
	}

	return nil
}

func (b *Backend) TagCheckConflict(ctx *Context, name string, category chara.CategoryID) error {
	qtr := tracer.Get(ctx.Context)
	done := qtr.Query("check for existing tag", false)
	dbtag, err := b.db.GetTagByName(ctx.Context, models.GetTagByNameParams{
		Name:       name,
		CategoryID: int64(category),
	})
	done()

	// this one is funky and may not be clear at first
	// because we specifically need it to report Tag doesn't exist
	if err != nil && !errors.Is(err, pgx.ErrNoRows) { // an actual error
		return err
	}
	// since we checked for errors that aren't ErrNoRows,
	if err != nil {
		// error not being nil means its ErrNoRows which is what we want
		return nil
	}

	// just for a slightly more complete error message
	done = qtr.Query("category", false)
	cat, err := b.db.GetCategory(ctx.Context, int64(category))
	if err != nil {
		return err
	}

	// it not being ErrNoRows means a conflict and thus is an error path
	return chara.NewError(chara.ErrorConflict,
		fmt.Sprintf(
			"Tag `%v` Already exists within Category `%v` #%v",
			dbtag.Name, cat.Name, category.String(),
		),
	)
}

func (b *Backend) CategoryCheckConflict(ctx *Context, name string) error {
	qtr := tracer.Get(ctx.Context)
	done := qtr.Query("check for existing tag", false)
	_, err := b.db.GetCategoryByName(ctx.Context, name)
	done()

	// the category does not already exist (what we want)
	if errors.Is(err, pgx.ErrNoRows) {
		return nil
	}
	// an actual error
	if err != nil {
		return err
	}

	return chara.NewError(chara.ErrorConflict,
		fmt.Sprintf(
			"Category `%v` Already exists",
			name,
		),
	)
}
