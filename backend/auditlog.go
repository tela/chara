package backend

import (
	"unsafe"

	"codeberg.org/jande/csid"
	"codeberg.org/tela/chara"
	"codeberg.org/tela/chara/database/models"
	"codeberg.org/tela/chara/internal/tracer"
	"github.com/jackc/pgtype"
)

// the danger function lmao
// do make sure F and T are compatible types, because this function is unsafe
func swapArrayType[F, T any](f []F) []T {
	return *(*[]T)(unsafe.Pointer(&f))
}

func csidConv[T ~uint64](ids ...T) []csid.ID {
	return swapArrayType[T, csid.ID](ids) // bitch
}

func (b *Backend) AuditLogGet(ctx *Context, id chara.AuditEntryID) (*chara.AuditLogEntry, error) {
	if err := b.Auth.UserCanDo(ctx.Context, ctx.UserID, chara.PermissionAccessAuditLog); err != nil {
		return nil, err
	}

	qtr := tracer.Get(ctx.Context)
	dbdat, err := b.db.GetAuditEntry(ctx.Context, int64(id))
	if err != nil {
		return nil, chara.ToError(err)
	}
	done := qtr.Query("get audit entry", false)
	entry := &chara.AuditLogEntry{
		ID:      chara.AuditEntryID(dbdat.ID),
		Class:   chara.AuditClass(dbdat.Class),
		Actor:   chara.UserID(dbdat.Actor),
		Targets: swapArrayType[int64, csid.ID](dbdat.Targets), // bitch
	}
	done()
	err = dbdat.Changes.AssignTo(&entry.Changes)
	if err != nil {
		return nil, chara.ToError(err)
	}

	return entry, nil
}

func (b *Backend) AuditLogAdd(
	ctx *Context,
	class chara.AuditClass, targets []csid.ID, changes []chara.AuditLogChange,
) (chara.AuditEntryID, error) {
	tx := ctx.getQ(b)
	id := chara.NewAuditEntryID()

	c := pgtype.JSONB{}
	c.Set(changes)

	qtr := tracer.Get(ctx.Context)
	done := qtr.Query("new audit entry", false)
	err := tx.NewAuditEntry(ctx.Context, models.NewAuditEntryParams{
		ID:      int64(id),
		Class:   int16(class),
		Actor:   int64(ctx.UserID),
		Targets: swapArrayType[csid.ID, int64](targets),
		Changes: c,
	})
	done()
	if err != nil {
		return 0, chara.ToError(err)
	}
	return id, nil
}

type AuditLogSearchParams struct {
	Class   []chara.AuditClass
	Actor   chara.UserID
	Targets csid.ID

	PageSize int32
}

func (b *Backend) AuditLogSearch(ctx *Context, after chara.AuditEntryID, params AuditLogSearchParams) ([]chara.AuditLogEntry, error) {
	if err := b.Auth.UserCanDo(ctx.Context, ctx.UserID, chara.PermissionAccessAuditLog); err != nil {
		return nil, err
	}
	if params.PageSize == 0 {
		params.PageSize = 10
	}

	qtr := tracer.Get(ctx.Context)
	done := qtr.Query("get audit entries", false)

	dbdat, err := b.db.GetAuditEntries(ctx, models.GetAuditEntriesParams{
		Limit:     params.PageSize,
		Class:     swapArrayType[chara.AuditClass, int16](params.Class),
		ByClass:   len(params.Class) != 0,
		Actor:     int64(params.Actor),
		ByActor:   !params.Actor.IsZero(),
		Targets:   int64(params.Targets),
		ByTargets: !params.Targets.IsZero(),

		AfterID: int64(after),
	})
	done()
	if err != nil {
		return nil, chara.ToError(err)
	}

	entries := make([]chara.AuditLogEntry, len(dbdat))

	for i, row := range dbdat {
		entries[i] = chara.AuditLogEntry{
			ID:      chara.AuditEntryID(row.ID),
			Class:   chara.AuditClass(row.Class),
			Actor:   chara.UserID(row.Actor),
			Targets: swapArrayType[int64, csid.ID](row.Targets),
		}
		err = row.Changes.AssignTo(&(entries[i]).Changes)
		if err != nil {
			return nil, chara.ToError(err)
		}
	}

	return entries, nil
}

func (b *Backend) AuditLogByTarget(ctx *Context, after chara.AuditEntryID, target csid.ID) ([]chara.AuditLogEntry, error) {
	if err := b.Auth.UserCanDo(ctx.Context, ctx.UserID, chara.PermissionAccessAuditLog); err != nil {
		return nil, err
	}

	qtr := tracer.Get(ctx.Context)
	done := qtr.Query("get audit entries by target", false)

	dbdat, err := b.db.GetAuditEntriesByTarget(ctx, models.GetAuditEntriesByTargetParams{
		Limit:  25,
		Target: int64(target),

		AfterID: int64(after),
	})
	done()
	if err != nil {
		return nil, chara.ToError(err)
	}

	entries := make([]chara.AuditLogEntry, len(dbdat))

	for i, row := range dbdat {
		entries[i] = chara.AuditLogEntry{
			ID:      chara.AuditEntryID(row.ID),
			Class:   chara.AuditClass(row.Class),
			Actor:   chara.UserID(row.Actor),
			Targets: swapArrayType[int64, csid.ID](row.Targets),
		}
		err = row.Changes.AssignTo(&(entries[i]).Changes)
		if err != nil {
			return nil, chara.ToError(err)
		}
	}

	return entries, nil
}

func (b *Backend) AuditCreate(ctx *Context, class chara.AuditClass, id csid.ID, obj any) (chara.AuditEntryID, error) {
	return b.AuditLogAdd(ctx, class, []csid.ID{id}, []chara.AuditLogChange{{Name: "", After: obj}})
}
func (b *Backend) AuditDelete(ctx *Context, class chara.AuditClass, id csid.ID, obj any) (chara.AuditEntryID, error) {
	return b.AuditLogAdd(ctx, class, []csid.ID{id}, []chara.AuditLogChange{{Name: "", Before: obj}})
}
