package backend

import (
	"codeberg.org/tela/chara"
	"codeberg.org/tela/chara/database/models"
	"codeberg.org/tela/chara/internal/tracer"
	"github.com/jackc/pgx/v4"
)

func (b *Backend) CategoryCreate(ctx *Context, cat chara.Category) (*chara.Category, error) {
	err := b.Auth.UserCanDo(ctx.Context, ctx.UserID, chara.PermissionManageCategories)
	if err != nil {
		return nil, err
	}
	tx, commit, err := ctx.GetTx(b, true)
	if err != nil {
		return nil, chara.ToError(err)
	}
	defer commit(false)

	cat.Name = NormaliseName(cat.Name)
	cat.Aliases = NormaliseNames(cat.Aliases)

	if err := b.CategoryCheckConflict(ctx, cat.Name); err != nil {
		return nil, err
	}

	cat.ID = chara.NewCategoryID()

	qtr := tracer.Get(ctx.Context)
	done := qtr.Query("add category", false)
	err = tx.NewCategory(ctx.Context, models.NewCategoryParams{
		ID:      int64(cat.ID),
		Name:    cat.Name,
		Aliases: cat.Aliases,
	})
	done()
	if err != nil {
		return nil, chara.ToError(err)
	}

	_, err = b.AuditCreate(ctx, chara.AuditClassCategoryCreate, cat.ID.Base(), []chara.AuditLogChange{{Name: "category", After: cat}})
	if err != nil {
		return nil, err
	}

	commit(true)
	return &cat, nil
}

func (b *Backend) CategoryList(ctx *Context, from chara.CategoryID) ([]*chara.Category, error) {
	qtr := tracer.Get(ctx.Context)
	done := qtr.Query("get categories", false)
	dbcats, err := b.db.ListCategoriesByID(ctx.Context, models.ListCategoriesByIDParams{
		ID:    int64(from),
		Limit: 50,
	})
	done()
	if err != nil {
		return nil, chara.ToError(err)
	}
	cats := make([]*chara.Category, len(dbcats))
	for i, dbcat := range dbcats {
		cats[i] = &chara.Category{
			ID:   chara.CategoryID(dbcat.ID),
			Name: dbcat.Name,
		}
	}

	return cats, nil
}

func (b *Backend) CategoryGet(ctx *Context, id chara.CategoryID) (*chara.Category, error) {
	qtr := tracer.Get(ctx.Context)
	done := qtr.Query("get category", false)
	dbcat, err := b.db.GetCategory(ctx.Context, int64(id))
	done()
	if err != nil {
		if err == pgx.ErrNoRows {
			return nil, chara.NewError(chara.ErrorNotFound, "Category "+id.String()+" Not Found")
		}
		return nil, chara.ToError(err)
	}

	return &chara.Category{
		ID:   chara.CategoryID(dbcat.ID),
		Name: dbcat.Name,
	}, nil
}
