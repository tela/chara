package backend

import (
	"codeberg.org/tela/chara"
	"codeberg.org/tela/chara/database/models"
	"codeberg.org/tela/chara/internal/tracer"
	"github.com/jackc/pgx/v4"
)

func (b *Backend) TagCreate(ctx *Context, name string, aliases []string, category chara.CategoryID) (*chara.Tag, error) {
	err := b.Auth.UserCanDo(ctx.Context, ctx.UserID, chara.PermissionManageTags)
	if err != nil {
		return nil, err
	}

	name = NormaliseName(name)
	aliases = NormaliseNames(aliases)

	if err := b.TagValidateNames(name, aliases); err != nil {
		return nil, err
	}
	if err := b.CategoryExists(ctx, category); err != nil {
		return nil, err
	}
	if err := b.TagCheckConflict(ctx, name, category); err != nil {
		return nil, err
	}

	tag := chara.Tag{
		ID:         chara.NewTagID(),
		CategoryID: category,
		Name:       name,
		Aliases:    aliases,
	}

	qtr := tracer.Get(ctx.Context)
	tx, commit, err := ctx.GetTx(b, true)
	if err != nil {
		return nil, chara.ToError(err)
	}
	defer commit(false)

	done := qtr.Query("add tag", false)
	err = tx.NewTag(ctx.Context, models.NewTagParams{
		ID:         int64(tag.ID),
		CategoryID: int64(tag.CategoryID),
		Name:       tag.Name,
		Aliases:    tag.Aliases,
	})
	done()
	if err != nil {
		return nil, chara.ToError(err)
	}

	_, err = b.AuditCreate(ctx, chara.AuditClassTagCreate, tag.ID.Base(), tag)
	if err != nil {
		return nil, err
	}

	commit(true)
	return &tag, nil
}

func (b *Backend) TagList(ctx *Context, from chara.TagID) ([]*chara.Tag, error) {
	qtr := tracer.Get(ctx.Context)
	done := qtr.Query("get tags", false)
	dbtags, err := b.db.ListTagsByID(ctx.Context, models.ListTagsByIDParams{
		ID:    int64(from),
		Limit: 50,
	})
	done()
	if err != nil {
		return nil, chara.ToError(err)
	}
	tags := make([]*chara.Tag, len(dbtags))
	for i, dbtag := range dbtags {
		tags[i] = &chara.Tag{
			ID:         chara.TagID(dbtag.ID),
			CategoryID: chara.CategoryID(dbtag.CategoryID),
			Name:       dbtag.Name,
			Aliases:    dbtag.Aliases,
		}
	}

	return tags, nil
}

func (b *Backend) TagSearch(ctx *Context, query string, limit int) ([]*chara.Tag, error) {
	qtr := tracer.Get(ctx.Context)
	done := qtr.Query("get tags", false)
	dbtags, err := b.db.GetTagsByName(ctx.Context, models.GetTagsByNameParams{
		Limit: int32(limit),
		Name:  query,
	})
	done()
	if err != nil {
		return nil, chara.ToError(err)
	}

	tags := make([]*chara.Tag, len(dbtags))
	for i, dbtag := range dbtags {
		tags[i] = &chara.Tag{
			ID:         chara.TagID(dbtag.ID),
			CategoryID: chara.CategoryID(dbtag.CategoryID),
			Name:       dbtag.Name,
			Aliases:    dbtag.Aliases,
		}
	}

	return tags, nil

}

func (b *Backend) TagGet(ctx *Context, id chara.TagID) (*chara.Tag, error) {
	qtr := tracer.Get(ctx.Context)
	done := qtr.Query("get tag", false)
	dbtag, err := b.db.GetTagByID(ctx.Context, int64(id))
	done()
	if err != nil {
		if err == pgx.ErrNoRows {
			return nil, chara.NewError(chara.ErrorNotFound, "Tag #"+id.String()+" Not Found")
		}
		return nil, chara.ToError(err)
	}

	return &chara.Tag{
		ID:         chara.TagID(dbtag.ID),
		CategoryID: chara.CategoryID(dbtag.CategoryID),
		Name:       dbtag.Name,
		Aliases:    dbtag.Aliases,
	}, nil
}
