package chara

import "codeberg.org/jande/csid"

type AuditClass uint16

const (
	AuditClassOther AuditClass = iota

	AuditClassCategoryOther AuditClass = 100 + iota
	AuditClassCategoryCreate
	AuditClassCategoryEdit
	AuditClassCategoryDelete

	AuditClassTagOther AuditClass = 200 + iota
	AuditClassTagCreate
	AuditClassTagEdit
	AuditClassTagDelete

	AuditClassCreatorOther AuditClass = 300 + iota
	AuditClassCreatorCreate
	AuditClassCreatorEditMetadata
	AuditClassCreatorAssignTags
	AuditClassCreatorPrivatize
	AuditClassCreatorDelete
)

type AuditLogEntry struct {
	ID      AuditEntryID `json:"id"`
	Class   AuditClass   `json:"class"`
	Actor   UserID       `json:"actor_id"`
	Targets []csid.ID    `json:"targets"`

	Changes []AuditLogChange `json:"changes"`
}

type AuditLogChange struct {
	Name string `json:"name"`

	Before any `json:"before"`
	After  any `json:"after"`

	Added   []any `json:"added"`
	Removed []any `json:"removed"`
}
