package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"

	"codeberg.org/tela/chara/apiserver"
	"codeberg.org/tela/chara/backend"
	"codeberg.org/tela/chara/frontend"

	_ "net/http/pprof"
)

type Config struct {
	Frontend frontend.Config
	Backend  backend.Config
	API      apiserver.Config
}

func main() {
	log.SetFlags(log.Flags() | log.Lshortfile)

	dsn := os.Args[1]
	var c = Config{
		Backend: backend.Config{
			Database: struct{ ConnString string }{dsn},
		},
	}

	be := &backend.Backend{}
	err := be.Init(context.TODO(), c.Backend)
	if err != nil {
		panic(err)
	}

	fe := &frontend.Frontend{}
	err = fe.Init(be, c.Frontend)
	if err != nil {
		panic(err)
	}
	api := &apiserver.API{}
	err = api.Init(be, c.API)
	if err != nil {
		panic(err)
	}

	http.Handle("/", apiserver.CORS(fe.R))
	http.Handle("/api/", http.StripPrefix("/api", api.R))

	fmt.Println("listening on :6555")
	if err := http.ListenAndServe(":6555", nil); err != nil {
		log.Println(err)
	}
}
