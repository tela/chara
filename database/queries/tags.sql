-- name: GetTagByID :one
SELECT * FROM tags
	WHERE id = $1
;

-- name: GetTagByName :one
SELECT * FROM tags
	WHERE name ILIKE $1 AND category_id = $2
;

-- name: NewTag :exec
INSERT INTO tags
	(id, name, category_id, aliases)
	VALUES ($1, $2, $3, $4)
;

-- name: UpdateTagName :exec
UPDATE tags
	SET name = $2, aliases = $3
	WHERE ID = $1
;

-- name: UpdateTagCategory :exec
UPDATE tags
	SET category_id = $2
	WHERE id = $1
;

-- name: ListTagsByID :many
SELECT * FROM tags
	WHERE id > $1
	ORDER BY id ASC
	LIMIT $2
;

-- name: GetTagsByName :many
SELECT * FROM tags
	WHERE name ILIKE $1
	ORDER BY name ASC
	LIMIT $2
;