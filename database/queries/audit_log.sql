-- name: GetAuditEntry :one
SELECT * FROM audit_log
	WHERE id = @id
;

-- name: NewAuditEntry :exec
INSERT INTO audit_log (id, class, actor, targets, changes)
	VALUES (@id, @class, @actor, @targets, @changes)
;

-- name: GetAuditEntries :many
SELECT * FROM audit_log
	WHERE id > @after_id
	AND NOT @by_class::boolean OR class = ANY (@class::smallint[])
	AND NOT @by_actor::boolean OR actor = @actor
	AND NOT @by_targets::boolean OR @targets::bigint = ANY (targets)
	ORDER BY id desc
	LIMIT $1
;

-- name: GetAuditEntriesByTarget :many
SELECT * FROM audit_log
	WHERE @target::bigint = ANY (targets)
	AND id > @after_id
	ORDER BY id desc
	LIMIT $1
;