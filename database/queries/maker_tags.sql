-- name: MTGetOne :one
SELECT * FROM maker_tags
	WHERE maker_id = $1 AND tag_id = $2
;

-- name: MTGetAll :exec
SELECT * FROM maker_tags
	WHERE maker_id = $1 AND tag_id = $2 AND positive = $3
	ORDER BY category_id, tag_id DESC
;

-- name: AssignTag :exec
INSERT INTO maker_tags (maker_id, tag_id, positive)
	VALUES ($1, $2, $3)
	ON CONFLICT (maker_id, tag_id) DO 
		UPDATE SET positive = $3
;

-- name: UnassignTag :exec
DELETE FROM maker_tags
	WHERE maker_id = $1 AND tag_id = $2
;