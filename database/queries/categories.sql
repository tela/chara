-- name: GetCategory :one
SELECT * FROM categories
	WHERE id = $1
;

-- name: GetCategoryByName :one
SELECT * FROM categories
	WHERE name ILIKE $1
;

-- name: NewCategory :exec
INSERT INTO categories (id, name, aliases)
	VALUES ($1, $2, $3)
;

-- name: UpdateCategoryName :exec
UPDATE categories
	SET name = $2, aliases = $3
	WHERE id = $1
;

-- name: DeleteCategory :exec
DELETE FROM categories
	WHERE id = $1
;

-- name: ListCategoriesByID :many
SELECT * FROM categories
	WHERE ID > $1
	ORDER BY id DESC
	LIMIT $2
-- TODO consider database function for fetching pagination information :D
;

-- name: ListCategoriesByName :many
SELECT * FROM categories
	WHERE name ILIKE $1
	ORDER BY name ASC
;