-- name: SearchTags :many
SELECT id, type, remote_id FROM makers
	WHERE tags @@ plainto_tsquery(@Query::text)
	ORDER BY id
	LIMIT $1
;