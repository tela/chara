-- name: GetMakerByID :one
SELECT * FROM makers
	WHERE id = $1
;

-- name: GetMakerByRemote :one
SELECT * FROM makers
	WHERE "type" = @Type AND remote_id = @RemoteID
;

-- name: NewMaker :exec
INSERT INTO makers
	(id, "type", remote_id)
	VALUES (@ID, @Type, @RemoteID)
;

-- name: ListMakersByID :many
SELECT id, type, remote_id FROM makers
	WHERE id < $1
	LIMIT $2
;

-- name: GetUntaggedMakers :many
SELECT * FROM makers AS m
	WHERE id < $1 AND (
			tags = null 
		OR (SELECT COUNT(*) FROM maker_tags AS mt WHERE maker_id = m.id) = 0)
	ORDER BY m.id DESC
	LIMIT $2
;