// Code generated by sqlc. DO NOT EDIT.
// versions:
//   sqlc v1.18.0
// source: makers.sql

package models

import (
	"context"
)

const getMakerByID = `-- name: GetMakerByID :one
SELECT id, type, remote_id, tags FROM makers
	WHERE id = $1
`

func (q *Queries) GetMakerByID(ctx context.Context, id int64) (Maker, error) {
	row := q.db.QueryRow(ctx, getMakerByID, id)
	var i Maker
	err := row.Scan(
		&i.ID,
		&i.Type,
		&i.RemoteID,
		&i.Tags,
	)
	return i, err
}

const getMakerByRemote = `-- name: GetMakerByRemote :one
SELECT id, type, remote_id, tags FROM makers
	WHERE "type" = $1 AND remote_id = $2
`

type GetMakerByRemoteParams struct {
	Type     int16
	Remoteid string
}

func (q *Queries) GetMakerByRemote(ctx context.Context, arg GetMakerByRemoteParams) (Maker, error) {
	row := q.db.QueryRow(ctx, getMakerByRemote, arg.Type, arg.Remoteid)
	var i Maker
	err := row.Scan(
		&i.ID,
		&i.Type,
		&i.RemoteID,
		&i.Tags,
	)
	return i, err
}

const getUntaggedMakers = `-- name: GetUntaggedMakers :many
SELECT id, type, remote_id, tags FROM makers AS m
	WHERE id < $1 AND (
			tags = null 
		OR (SELECT COUNT(*) FROM maker_tags AS mt WHERE maker_id = m.id) = 0)
	ORDER BY m.id DESC
	LIMIT $2
`

type GetUntaggedMakersParams struct {
	ID    int64
	Limit int32
}

func (q *Queries) GetUntaggedMakers(ctx context.Context, arg GetUntaggedMakersParams) ([]Maker, error) {
	rows, err := q.db.Query(ctx, getUntaggedMakers, arg.ID, arg.Limit)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	var items []Maker
	for rows.Next() {
		var i Maker
		if err := rows.Scan(
			&i.ID,
			&i.Type,
			&i.RemoteID,
			&i.Tags,
		); err != nil {
			return nil, err
		}
		items = append(items, i)
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return items, nil
}

const listMakersByID = `-- name: ListMakersByID :many
SELECT id, type, remote_id FROM makers
	WHERE id < $1
	LIMIT $2
`

type ListMakersByIDParams struct {
	ID    int64
	Limit int32
}

type ListMakersByIDRow struct {
	ID       int64
	Type     int16
	RemoteID string
}

func (q *Queries) ListMakersByID(ctx context.Context, arg ListMakersByIDParams) ([]ListMakersByIDRow, error) {
	rows, err := q.db.Query(ctx, listMakersByID, arg.ID, arg.Limit)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	var items []ListMakersByIDRow
	for rows.Next() {
		var i ListMakersByIDRow
		if err := rows.Scan(&i.ID, &i.Type, &i.RemoteID); err != nil {
			return nil, err
		}
		items = append(items, i)
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return items, nil
}

const newMaker = `-- name: NewMaker :exec
INSERT INTO makers
	(id, "type", remote_id)
	VALUES ($1, $2, $3)
`

type NewMakerParams struct {
	ID       int64
	Type     int16
	Remoteid string
}

func (q *Queries) NewMaker(ctx context.Context, arg NewMakerParams) error {
	_, err := q.db.Exec(ctx, newMaker, arg.ID, arg.Type, arg.Remoteid)
	return err
}
