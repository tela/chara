-- +migrate Up
CREATE TABLE public.categories
(
	id bigint NOT NULL,
	name text NOT NULL,
	aliases TEXT[],
	PRIMARY KEY (id),
	UNIQUE (name)
);

INSERT INTO categories (id, name) VALUES (0, '');

CREATE TABLE public.tags
(
	id bigint NOT NULL,
	category_id bigint NOT NULL DEFAULT 0,
	name text NOT NULL,
	aliases text[] NOT NULL,
	PRIMARY KEY (id),
	UNIQUE (name, category_id),
	FOREIGN KEY (category_id)
		REFERENCES public.categories (id) MATCH SIMPLE
		ON UPDATE SET DEFAULT
		ON DELETE SET DEFAULT
		NOT VALID
);

CREATE TABLE public.makers
(
	id bigint NOT NULL,
	type smallint NOT NULL,
	remote_id text NOT NULL,
	tags tsvector,
	PRIMARY KEY (id),
	UNIQUE (type, remote_id)
);

CREATE TABLE public.maker_tags
(
	maker_id bigint NOT NULL,
	tag_id bigint NOT NULL,
	positive boolean NOT NULL,
	PRIMARY KEY (maker_id, tag_id),
	FOREIGN KEY (maker_id)
		REFERENCES public.makers (id) MATCH SIMPLE
		ON UPDATE CASCADE
		ON DELETE CASCADE
		NOT VALID,
	FOREIGN KEY (tag_id)
		REFERENCES public.tags (id) MATCH SIMPLE
		ON UPDATE CASCADE
		ON DELETE CASCADE
		NOT VALID
);

CREATE OR REPLACE FUNCTION public.update_maker_tag_index()
	RETURNS trigger
	LANGUAGE 'plpgsql'
	 NOT LEAKPROOF
AS $BODY$
BEGIN
	UPDATE MAKERS
		SET tags = (SELECT string_agg(tag_id::text, ', ') FROM maker_tags WHERE maker_id = OLD.maker_id AND positive = true)::tsvector
		WHERE id = OLD.maker_id;
	RETURN null;
END
$BODY$;

CREATE TRIGGER maker_tags_sync_tag_tsvector
	AFTER INSERT OR DELETE OR UPDATE 
	ON public.maker_tags
	FOR EACH ROW
	EXECUTE PROCEDURE public.update_maker_tag_index();


CREATE FUNCTION public.category_delete_rename()
	RETURNS trigger
	LANGUAGE 'plpgsql'
	 NOT LEAKPROOF
AS $BODY$
BEGIN
	UPDATE public.tags
		SET name = name || '~' || OLD.name || '~'
		WHERE category_id = OLD.id;
	RETURN null;
END
$BODY$;

CREATE TRIGGER category_delete_rename_tags
	BEFORE DELETE
	ON public.categories
	FOR EACH ROW
	EXECUTE PROCEDURE public.category_delete_rename();

CREATE TABLE public.audit_log
(
    id bigint NOT NULL,
    class smallint NOT NULL,
    actor bigint NOT NULL,
    targets bigint[] NOT NULL,
    changes jsonb NOT NULL,
    PRIMARY KEY (id)
);