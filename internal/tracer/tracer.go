package tracer

import (
	"context"
	"fmt"
	"path/filepath"
	"runtime"
	"strconv"
	"strings"
	"sync"
	"time"

	"codeberg.org/tela/chara/internal/stopwatch"
)

// ugly debug abomination

func With(ctx context.Context) context.Context {
	return context.WithValue(ctx, QueryKey, &QueryStat{
		queries: make([]QueryTime, 0),
	})
}

type keyType struct{}

var QueryKey keyType

func Get(ctx context.Context) *QueryStat {
	v := ctx.Value(QueryKey)
	fmt.Printf("get query stat %p", v)
	if v == nil {
		fmt.Println(" none")
		return &QueryStat{placeholder: true}
	}
	fmt.Println(" is one")

	return v.(*QueryStat)
}

type QueryStat struct {
	mu      sync.Mutex
	count   uint
	queries []QueryTime

	placeholder bool
}

type QueryTime struct {
	Name string
	Func string
	File string
	Time time.Duration
	Tx   bool
}

func (q *QueryStat) Query(name string, meta bool) (finish func()) {
	fmt.Println("call to query:")
	for i := 1; ; i++ {
		pc, _, li, ok := runtime.Caller(i)
		if !ok {
			break
		}

		fn := runtime.FuncForPC(pc)
		funna := fn.Name()

		if !strings.HasPrefix(funna, "codeberg.org/tela/") {
			break
		}

		fmt.Printf("\t%v :%v\n", funna, li)
	}
	fmt.Println("")

	pc, fl, li, _ := runtime.Caller(1)
	fn := runtime.FuncForPC(pc)
	funna := fn.Name()
	funna = funna[strings.LastIndex(funna, "/"):]

	st := stopwatch.NewAndStart()
	return func() {
		q.mu.Lock()
		defer q.mu.Unlock()

		d := st.Stop()
		q.count++
		if q.placeholder {
			fmt.Println("\t", funna, d)
			return
		}
		q.queries = append(q.queries, QueryTime{name, funna, filepath.Base(fl) + ":" + strconv.Itoa(li), d, meta})
	}
}

func (q *QueryStat) String() string {
	fmt.Println("q stat string call")

	if q.count == 0 {
		return ""
	}

	q.mu.Lock()
	defer q.mu.Unlock()

	var totaldur time.Duration

	for _, x := range q.queries {
		totaldur += x.Time
	}

	avgdur := totaldur / time.Duration(len(q.queries))

	s := &strings.Builder{}
	fmt.Fprintf(s, "Queries: %v, Total Time: %v", q.count, totaldur.Round(time.Millisecond))
	if q.count > 1 {
		fmt.Fprintf(s, ", Average Time: %v", avgdur.Round(time.Millisecond))
	}
	s.WriteRune('\n')

	cf := ""
	var total time.Duration
	var count int
	for _, x := range q.queries {
		if x.Func != cf {
			if count >= 1 {
				fmt.Fprintf(s, " ^ C: %v, TT: %v, AT: %v\n", count, total.Round(time.Millisecond), (total / time.Duration(count)).Round(time.Millisecond))
			} else {
				fmt.Fprintln(s)
			}
			fmt.Fprintf(s, "%v (%v)\n", x.Func, x.File)
			cf = x.Func
			total = 0
			count = 0
		}
		count++
		total += x.Time
		let := "Q"
		if x.Tx {
			let = "M"
		}
		fmt.Fprintf(s, " | %v: %v %v\n", x.Name, let, x.Time.Round(time.Millisecond))
	}
	if count >= 1 {
		fmt.Fprintf(s, " ^ C: %v, TT: %v, AT: %v\n", count, total.Round(time.Millisecond), (total / time.Duration(count)).Round(time.Millisecond))
	} else {
		fmt.Fprintln(s)
	}

	return s.String()
}
