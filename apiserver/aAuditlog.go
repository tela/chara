package apiserver

import (
	"codeberg.org/jande/csid"
	"codeberg.org/tela/chara"
	"codeberg.org/tela/chara/backend"
	"github.com/go-chi/chi/v5"
)

func (a *API) routesAuditlog(r chi.Router) {
	r.Get("/{id}", wrapper(a.getAuditlogGet))
	r.Get("/search", wrapper(a.getAuditlogSearch))
	r.Get("/about/{target}", wrapper(a.getAuditlogTarget))
}

type getAuditlogGetParams struct {
	ID chara.AuditEntryID `param:"id"`
}

// GET AuditlogGet
func (a *API) getAuditlogGet(ctx *Context, params getAuditlogGetParams) (any, error) {
	return a.b.AuditLogGet(ctx.Context, params.ID)
}

type getAuditlogSearchParams struct {
	After    chara.AuditEntryID `query:"after"`
	PageSize int32              `query:"limit"`

	Class   []chara.AuditClass `query:"class"`
	Actor   chara.UserID       `query:"actor"`
	Targets csid.ID            `query:"target"`
}

// GET AuditlogSearch
func (a *API) getAuditlogSearch(ctx *Context, params getAuditlogSearchParams) (any, error) {
	return a.b.AuditLogSearch(ctx.Context, params.After, backend.AuditLogSearchParams{
		Class:   params.Class,
		Actor:   params.Actor,
		Targets: params.Targets,

		PageSize: params.PageSize,
	})
}

type getAuditlogTargetParams struct {
	Target   csid.ID            `param:"target"`
	After    chara.AuditEntryID `query:"after"`
	PageSize int32              `query:"limit"`
}

// GET AuditlogTarget
func (a *API) getAuditlogTarget(ctx *Context, params getAuditlogTargetParams) (any, error) {
	return a.b.AuditLogByTarget(ctx.Context, params.After, params.Target)
}
