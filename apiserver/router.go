package apiserver

import (
	"codeberg.org/tela/chara/backend"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
)

type Config struct {
}

type API struct {
	R chi.Router
	b *backend.Backend
}

func (a *API) Init(b *backend.Backend, c Config) error {
	a.b = b
	r := chi.NewRouter()
	a.R = r
	r.Use(
		middleware.Recoverer,
		middleware.DefaultLogger,
		CORS,
		Tracer,
		a.GetAuth,
	)
	r.With(
		middleware.Heartbeat("/ping"),
	)

	r.Route("/v1/tag", a.routesTag)
	r.Route("/v1/category", a.routesCategory)
	r.Route("/v1/meta", a.routesMeta)
	r.Route("/v1/auditlog", a.routesAuditlog)

	return nil
}

// :zzz_pocky_knife:
//func CORS(w http.ResponseWriter, r *http.Request) {
//	w.Header().Add("Access-Control-Allow-Origin", "*")
//	w.Header().Add("Access-Control-Allow-Method", "HEAD, GET, POST, DELETE")
//	w.Header().Add("Access-Control-Allow-Headers", "Content-Type, Authorization")
//	w.WriteHeader(200)
//}
