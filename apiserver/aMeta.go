package apiserver

import (
	"codeberg.org/tela/chara"
	"github.com/go-chi/chi/v5"
)

func (a *API) routesMeta(r chi.Router) {
	r.Get("/me", wrapper(a.getMetaMe))
	r.Get("/stringify-permissions/{perms}", wrapper(a.getMetaStringifyPermissions))
}

type getMetaMeParams struct {
}

// GET MetaMe
func (a *API) getMetaMe(ctx *Context, params getMetaMeParams) (any, error) {
	perm := getAuth(ctx.Context.Context)
	return chara.User{
		ID:          chara.NewUserID(),
		Name:        "Nyan",
		Permissions: perm.Perms,
	}, nil
}

type getMetaStringifyPermissionsParams struct {
	Permissions chara.Permissions `param:"perms"`
}

// GET MetaStringifyPermissions
func (a *API) getMetaStringifyPermissions(ctx *Context, params getMetaStringifyPermissionsParams) (any, error) {
	return struct {
		Str string `json:"str"`
	}{Str: params.Permissions.String()}, nil
}
