package apiserver

import (
	"codeberg.org/tela/chara"
	"github.com/go-chi/chi/v5"
)

func (a *API) routesTag(r chi.Router) {
	r.Get("/{id}", wrapper(a.getTagID))
}

type getTagIDParams struct {
	ID chara.TagID `param:"id"`
}

// GET TagID
func (a *API) getTagID(ctx *Context, params getTagIDParams) (any, error) {
	return a.b.TagGet(ctx.Context, params.ID)
}
