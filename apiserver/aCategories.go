package apiserver

import (
	"codeberg.org/tela/chara"
	"github.com/go-chi/chi/v5"
)

func (a *API) routesCategory(r chi.Router) {
	r.Get("/{id}", wrapper(a.getCategoryID))
	r.Get("/list", wrapper(a.getCategoryList))
	r.Post("/", wrapperPost(a.postCategory))
}

type getCategoryIDParams struct {
	ID chara.CategoryID `param:"id"`
}

// GET CategoryID
func (a *API) getCategoryID(ctx *Context, params getCategoryIDParams) (any, error) {
	return a.b.CategoryGet(ctx.Context, params.ID)
}

type getCategoryListParams struct {
	From chara.CategoryID `query:"from"`
}

// GET CategoryList
func (a *API) getCategoryList(ctx *Context, params getCategoryListParams) (any, error) {
	return a.b.CategoryList(ctx.Context, params.From)
}

type postCategoryParams struct {
}

type postCategoryBody struct {
	Name    string   `json:"name"`
	Aliases []string `json:"aliases"`
}

// POST Category
func (a *API) postCategory(ctx *Context, params postCategoryParams, body postCategoryBody) (any, error) {
	return a.b.CategoryCreate(ctx.Context, chara.Category{
		Name:    body.Name,
		Aliases: body.Aliases,
	})
}
