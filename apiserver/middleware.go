package apiserver

import (
	"context"
	"net/http"

	"codeberg.org/tela/chara"
	"codeberg.org/tela/chara/internal/tracer"
)

type ctxAuth struct {
	UserID chara.UserID
	Perms  chara.Permissions
}

var ctxAuthKey = struct{}{}

func getAuth(ctx context.Context) *ctxAuth {
	return ctx.Value(ctxAuthKey).(*ctxAuth)
}

func (a *API) GetAuth(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// TODO actual auth stuff
		var whom chara.UserID

		perm, err := a.b.Auth.GetPermissions(r.Context(), whom)
		if err != nil {
			replyError(w, chara.NewError(chara.ErrorMissingIdentity, err))
			return
		}

		r = r.WithContext(context.WithValue(r.Context(), ctxAuthKey, &ctxAuth{
			UserID: whom,
			Perms:  perm,
		}))

		next.ServeHTTP(w, r)
	})
}

func Tracer(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		r = r.WithContext(tracer.With(r.Context()))

		next.ServeHTTP(w, r)
	})
}

func CORS(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Access-Control-Allow-Origin", "*")
		w.Header().Add("Access-Control-Allow-Method", "HEAD, GET, POST, DELETE")
		w.Header().Add("Access-Control-Allow-Headers", "Content-Type, Authorization")

		if r.Method == "OPTIONS" {
			w.WriteHeader(200)
			return
		}

		next.ServeHTTP(w, r)
	})
}
