package apiserver

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"math/rand"
	"net/http"
	"net/url"
	"strconv"

	"codeberg.org/tela/chara"
	"codeberg.org/tela/chara/backend"
	"codeberg.org/tela/chara/internal/tracer"
	"github.com/go-chi/chi/v5"
	"github.com/monoculum/formam/v3"
)

const (
	ReaderCap = 1000 * 1000 * 5 // cap requests to 5 megabytes
)

type Context struct {
	*backend.Context
	// will contain stuff like auth shenanigans in the future

	w http.ResponseWriter
	r *http.Request

	hasBody bool

	pageSession uint64
}

func wrapperPost[Body, Params any](fn func(ctx *Context, params Params, reqData Body) (any, error)) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		ctx, err := getContext(w, r)
		if err != nil {
			replyError(w, err)
			return
		}

		p, err := parseParams[Params](r)
		if err != nil {
			replyError(w, err)
			return
		}

		data, err := parseBody[Body](ctx)
		if err != nil {
			replyError(w, chara.NewError(chara.ErrorMalformedJSON, err))
			return
		}

		dat, err := fn(ctx, p, data)
		if err != nil {
			replyError(w, err)
			return
		}

		fmt.Println(tracer.Get(ctx.Context.Context).String())

		jw := json.NewEncoder(w)
		er := jw.Encode(dat)
		if er != nil {
			log.Println(er)
		}
	}
}

func wrapper[Params any](fn func(ctx *Context, params Params) (any, error)) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		ctx, err := getContext(w, r)
		if err != nil {
			replyError(w, err)
			return
		}

		p, err := parseParams[Params](r)
		if err != nil {
			replyError(w, err)
			return
		}

		dat, err := fn(ctx, p)
		if err != nil {
			replyError(w, err)
			return
		}

		fmt.Println(tracer.Get(ctx.Context.Context).String())

		jw := json.NewEncoder(w)
		er := jw.Encode(dat)
		if er != nil {
			log.Println(er)
		}
	}
}

func getContext(w http.ResponseWriter, r *http.Request) (*Context, error) {
	auth := getAuth(r.Context())
	if auth == nil {
		auth = &ctxAuth{}
	}

	ctx := &Context{
		w: w,
		r: r,

		Context: &backend.Context{
			UserID:  auth.UserID,
			Context: r.Context(),
		},

		pageSession: getSession(r),
	}

	return ctx, nil
}

func getSession(r *http.Request) (dbgSession uint64) {
	if r.Header.Get("page-session") != "" {
		ps, err := strconv.ParseUint(r.Header.Get("page-session"), 0, 0)
		if err == nil {
			dbgSession = ps
		} else {
			log.Println(err)
		}
	} else {
		dbgSession = rand.Uint64()
	}

	return
}

func parseBody[T any](ctx *Context) (T, error) {
	var data T

	if ctx.r.Body == nil {
		return data, nil
	}
	ctx.hasBody = true

	jr := json.NewDecoder(io.LimitReader(ctx.r.Body, ReaderCap))
	err := jr.Decode(&data)
	if err != nil {
		return data, err
	}
	return data, nil
}

var queryParser = formam.NewDecoder(&formam.DecoderOptions{
	TagName:           "query",
	IgnoreUnknownKeys: true,
})
var paramParser = formam.NewDecoder(&formam.DecoderOptions{
	TagName:           "param",
	IgnoreUnknownKeys: true,
})

func parseParams[T any](r *http.Request) (T, error) {
	var params T
	err := queryParser.Decode(r.URL.Query(), &params)
	if err != nil {
		return params, chara.NewError(chara.ErrorIssueWithInput, err)
	}

	chitx := chi.RouteContext(r.Context())

	urlparams := make(url.Values, len(chitx.URLParams.Keys))
	for i := range chitx.URLParams.Keys {
		k, v := chitx.URLParams.Keys[i], chitx.URLParams.Values[i]
		if urlparams[k] == nil {
			urlparams[k] = []string{v}
		} else {
			urlparams[k] = append(urlparams[k], v)
		}
	}

	err = paramParser.Decode(urlparams, &params)
	if err != nil {
		return params, chara.NewError(chara.ErrorIssueWithInput, err)
	}
	return params, nil
}

func replyError(w http.ResponseWriter, err error) {
	log.Println(err)
	nr := chara.ToError(err)
	w.WriteHeader(chara.MapError(nr.Code))
	jw := json.NewEncoder(w)
	er := jw.Encode(nr)
	if er != nil {
		log.Println(er)
	}
}
