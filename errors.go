package chara

import (
	"fmt"
	"runtime"
	"strings"
)

var StackTrace bool = true

type Error struct {
	Code    ErrorCode `json:"code"`
	Message string    `json:"message"`
	Crumbs  string    `json:"crumbs,omitempty"`
	Meta    any       `json:"meta,omitempty"`

	Source     string `json:"source,omitempty"`
	StackTrace string `json:"stacktrace,omitempty"`
}

func (e Error) Error() string {
	if e.Meta != nil {
		return fmt.Sprintf("%v: %v <%v>", e.Code, e.Message, e.Meta)
	}
	return fmt.Sprintf("%v: %v", e.Code, e.Message)
}

func NewError(code ErrorCode, meta any) *Error {
	return newError(code, meta, 2)
}

func ToError(err error) *Error {
	if cherr, ok := err.(*Error); ok {
		return cherr
	}
	return newError(ErrorInternalError, err.Error(), 2)
}

func newError(code ErrorCode, meta any, skip int) *Error {
	er := Error{
		Code:    code,
		Message: code.String(),
		Meta:    meta,
	}

	pc, file, line, _ := runtime.Caller(skip)
	fn := runtime.FuncForPC(pc)
	file = strings.TrimPrefix(file, fileprefix)

	er.Source = fmt.Sprintf("%v:%v (%v)", file, line, fn.Name())

	if StackTrace {
		var b = make([]byte, 4000)
		s := runtime.Stack(b, false)
		fmt.Println(s)
		er.StackTrace = string(b[:s])
	}

	return &er
}

func (e *Error) AddMsg(s string) *Error {
	if e.Message != "" {
		e.Message += "; "
	}
	e.Message += s
	return e
}

func (e *Error) AddCrumb(s string) *Error {
	if e.Crumbs != "" {
		e.Crumbs += " <- "
	}
	e.Crumbs += s
	return e
}

func ErrorAddCrumb(err error, on string) error {
	if err == nil {
		return nil
	}
	apiErr, ok := err.(*Error)
	if !ok {
		return err
	}
	return apiErr.AddCrumb(on)
}

// this was here when i cloned the repo, utterly no idea what it was going to be used for
//type ErrorConstraint struct {
//	Message    string
//	Item       any
//	Constraint any
//}

var fileprefix string

func init() {
	_, file, _, _ := runtime.Caller(0)

	fileprefix = strings.TrimSuffix(file, "errors.go")
}
