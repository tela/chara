package chara

import "net/http"

type ErrorCode uint

//go:generate stringer -type=ErrorCode -trimprefix=Error -linecomment

const (
	ErrorUndefined          ErrorCode = 0    // Undefined Error
	ErrorInternalError      ErrorCode = 100  // Internal Error
	ErrorMissingPermissions ErrorCode = 5000 // Missing Permissions
	ErrorMissingIdentity    ErrorCode = 5001 // Missing Identity (Must Login)

	ErrorIssueWithInput       ErrorCode = 10000 + iota // Undefined Issue with Input
	ErrorMalformedJSON                                 // Malformed JSON Input
	ErrorNotFound                                      // Not Found
	ErrorConflict                                      // Conflict / Already Exists
	ErrorInputFailsValidation                          // Input Fails Validation
)

var ErrorHTTPMapping = map[ErrorCode]int{
	ErrorUndefined:          http.StatusTeapot, // this shouldn't happen
	ErrorInternalError:      http.StatusInternalServerError,
	ErrorMissingPermissions: http.StatusForbidden,
	ErrorMissingIdentity:    http.StatusUnauthorized,

	ErrorIssueWithInput:       http.StatusBadRequest,
	ErrorMalformedJSON:        http.StatusBadRequest,
	ErrorNotFound:             404,
	ErrorConflict:             http.StatusConflict,
	ErrorInputFailsValidation: http.StatusBadRequest,
}

func MapError(code ErrorCode) int {
	htcode := ErrorHTTPMapping[code]
	if htcode == 0 {
		htcode = http.StatusTeapot
	}

	return htcode
}
